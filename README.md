# Tempus themes for Highlight

Use `-s` flag directly or add it to the `HIGHLIGHT_OPTIONS` enironvment
variable.

```sh
# Apply the theme
highlight -s tempus_winter.theme file
```

## Contributing

All contributions should be submitted to the Tempus themes generator.
See its
[CONTRIBUTING.md](https://gitlab.com/protesilaos/tempus-themes-generator/blob/master/CONTRIBUTING.md).

## COPYING

GNU General Public License Version 3.
